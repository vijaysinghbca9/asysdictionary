package dictionary_services;

import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.AsyncTask;
import android.os.IBinder;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.androidbelieve.AsysDic.MainActivity;
import com.androidbelieve.AsysDic.MySingleton;
import com.androidbelieve.AsysDic.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by keyur on 26-01-2016.
 */
public class CopyMyTextService extends Service {
    private WindowManager windowManager;

    Context context;
    PopupWindow pwindo = null;
    LayoutInflater layoutInflater;
    View popupView;
    TextView txtSearch;
    @Override
    public void onCreate() {
        super.onCreate();
        //Toast.makeText(this, "Service Create", Toast.LENGTH_LONG).show();
        ClipboardManager clipBoard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        clipBoard.addPrimaryClipChangedListener(new ClipboardListener());
        context = this;
        layoutInflater = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        popupView = layoutInflater.inflate(R.layout.my_widget_provider, null);
        Button btnClose = (Button)popupView.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                windowManager.removeView(popupView);
            }
        });
        Button btnSearch = (Button)popupView.findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context,"Search " + MySingleton.getInstance().textCopiedValue,Toast.LENGTH_SHORT).show();
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("keysearch",MySingleton.getInstance().textCopiedValue);
                ComponentName cn = new ComponentName(context, MainActivity.class);
                intent.setComponent(cn);
                startActivity(intent);
                windowManager.removeView(popupView);
                }
        });
        txtSearch = (TextView)popupView.findViewById(R.id.txtSearch);
        pwindo = new PopupWindow(popupView, LinearLayout.LayoutParams.MATCH_PARENT,  LinearLayout.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Let it continue running until it is stopped.
        //Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
    }
    class ClipboardListener implements
            ClipboardManager.OnPrimaryClipChangedListener {
        public void onPrimaryClipChanged() {
            ClipboardManager clipBoard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            CharSequence pasteData = "";
            ClipData.Item item = clipBoard.getPrimaryClip().getItemAt(0);
            pasteData = item.getText();
            MySingleton.getInstance().textCopiedValue = "" + pasteData;
           // Toast.makeText(getApplicationContext(), "copied val=" + pasteData,
             //       Toast.LENGTH_SHORT).show();
//            RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.my_widget_provider);
//            ComponentName thisWidget = new ComponentName( context, MyWidgetProvider.class );
//            remoteViews.setTextViewText(R.id.txtSearch, "" + MySingleton.getInstance().textCopiedValue);
//            AppWidgetManager.getInstance(context).updateAppWidget( thisWidget, remoteViews );
            //
            new HttpAsyncTask().execute("http://infrasissolution.com/APIs/dictionaryapp/wordofmeaning.php",pasteData+"");
        }
    }
//    public static String GET(String url){
//        InputStream inputStream = null;
//        String result = "";
//        try {
//
//            // create HttpClient
//            HttpClient httpclient = new DefaultHttpClient();
//
//            // make GET request to the given URL
//            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
//
//            // receive response as inputStream
//            inputStream = httpResponse.getEntity().getContent();
//
//            // convert inputstream to string
//            if(inputStream != null)
//                result = convertInputStreamToString(inputStream);
//            else
//                result = "Did not work!";
//
//        } catch (Exception e) {
//            Log.d("InputStream", e.getLocalizedMessage());
//        }
//
//        return result;
//    }


    public static String GET(String murl,String text){
        InputStream inputStream = null;
        String result = "";
        URL url= null;
        HttpURLConnection urlConnection = null;
        String dataUrlParameters = "wordid="+text;
        HttpURLConnection connection = null;
        try {
// Create connection
            url = new URL(murl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
            connection.setRequestProperty("Content-Length","" + Integer.toString(dataUrlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
// Send request
            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.writeBytes(dataUrlParameters);
            wr.flush();
            wr.close();
// Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            String responseStr = response.toString();
            result = responseStr;

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            if (connection != null) {
                connection.disconnect();
            }
        }
        return result;
    }
    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0],urls[1]);
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            //Toast.makeText(getBaseContext(), "Received!", Toast.LENGTH_LONG).show();
            try {
                JSONObject jsonObject = new JSONObject(result);
                if(jsonObject.get("status").toString().equalsIgnoreCase("true")){

                    Object intervention = jsonObject.get("data");

                    if (intervention instanceof JSONArray) {
                        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
                        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                                WindowManager.LayoutParams.MATCH_PARENT,
                                WindowManager.LayoutParams.WRAP_CONTENT,
                                WindowManager.LayoutParams.TYPE_PHONE,
                                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                                PixelFormat.TRANSLUCENT);

                        params.gravity = Gravity.TOP | Gravity.LEFT;
                        params.x = 0;
                        params.y = 100;
                        //txtSearch.setText("" + MySingleton.getInstance().textCopiedValue);

                        String msg = "" + MySingleton.getInstance().textCopiedValue + " : " ;

                        txtSearch.setText("" + MySingleton.getInstance().textCopiedValue + " : No Data found");

                        windowManager.addView(popupView, params);
                    }
                    else if (intervention instanceof JSONObject) {
                        JSONObject jsonData = jsonObject.getJSONObject("data");
                        JSONArray similarJsonArray = jsonData.getJSONArray("similar");
                        if(similarJsonArray.length() > 0){
                            windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
                            WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                                    WindowManager.LayoutParams.MATCH_PARENT,
                                    WindowManager.LayoutParams.WRAP_CONTENT,
                                    WindowManager.LayoutParams.TYPE_PHONE,
                                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                                    PixelFormat.TRANSLUCENT);

                            params.gravity = Gravity.TOP | Gravity.LEFT;
                            params.x = 0;
                            params.y = 100;
                            //txtSearch.setText("" + MySingleton.getInstance().textCopiedValue);

                            String msg = "" + MySingleton.getInstance().textCopiedValue + " : " ;

                            txtSearch.setText("" + MySingleton.getInstance().textCopiedValue + " : " + similarJsonArray.getJSONObject(0).get("gloss").toString());

                            windowManager.addView(popupView, params);
                        }
                    }

                }
                else{
                    Toast.makeText(context,"Data not found!",Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            catch (Exception e){
                e.printStackTrace();
            }
//            JSONObject jsonObject = null;
//            try {
//                jsonObject = new JSONObject(result);
//                windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
//                WindowManager.LayoutParams params = new WindowManager.LayoutParams(
//                        WindowManager.LayoutParams.MATCH_PARENT,
//                        WindowManager.LayoutParams.WRAP_CONTENT,
//                        WindowManager.LayoutParams.TYPE_PHONE,
//                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
//                        PixelFormat.TRANSLUCENT);
//
//                params.gravity = Gravity.TOP | Gravity.LEFT;
//                params.x = 0;
//                params.y = 100;
//                //txtSearch.setText("" + MySingleton.getInstance().textCopiedValue);
//                txtSearch.setText("" + MySingleton.getInstance().textCopiedValue + " : " + jsonObject.get("title").toString());
//
//                windowManager.addView(popupView, params);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            catch (Exception e){
//
//            }
        }
    }
    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}