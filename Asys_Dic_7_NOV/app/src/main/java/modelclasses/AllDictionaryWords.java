package modelclasses;

/**
 * Created by omnipresent on 26/11/15.
 */
public class AllDictionaryWords {

    int word_id;
    String mkey,mValue;

    public AllDictionaryWords() {
    }

    public AllDictionaryWords(int id,String key,String value){
        this.word_id = id;
        this.mkey = key;
        this.mValue = value;
    }

    public int getWord_id() {
        return word_id;
    }

    public void setWord_id(int word_id) {
        this.word_id = word_id;
    }

    public String getMkey() {
        return mkey;
    }

    public void setMkey(String mkey) {
        this.mkey = mkey;
    }

    public String getmValue() {
        return mValue;
    }

    public void setmValue(String mValue) {
        this.mValue = mValue;
    }
}
