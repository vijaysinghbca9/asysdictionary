package modelclasses;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by omnipresent on 07/12/15.
 */
public class DataBaseHelperAdapter {
    public static final String DATABASE_NAME = "AsysDatabase";
    public static final int DATABASE_VERSION = 1;

    public static final String TABLE_NAME = "SavedWords";
    public static final String KEY_ID = "id";
    public static final String KEY_DATA = "data";

    public static final String CREATE_TABLE = "create table "+ TABLE_NAME + " ( " +
            KEY_ID + " integer primary key autoincrement , " + KEY_DATA + " text) ";

    public static final String WORD_TABLE_NAME = "AllWords";
    public static final String WORD_KEY_ID = "id";
    public static final String WORD_KEY_DATA = "data";
    public static final String CREATE_ALL_WORD = "create table "+WORD_TABLE_NAME+" ( "+
            WORD_KEY_ID + " integer primary key autoincrement , " + WORD_KEY_DATA + " text";

    private final Context context;
    private DatabaseOpenHelper dbhelper;
    private SQLiteDatabase db;

    public DataBaseHelperAdapter(Context cntx)
    {
        this.context = cntx;
        dbhelper = new DatabaseOpenHelper(context);
    }
    public class DatabaseOpenHelper extends SQLiteOpenHelper{
        public DatabaseOpenHelper(Context context)
        {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
        @Override
        public void onCreate(SQLiteDatabase db)
        {
            db.execSQL(CREATE_TABLE);
//            db.execSQL(CREATE_ALL_WORD);
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int arg1, int arg2)
        {
            db.execSQL("drop table if exists "+TABLE_NAME);
//            db.execSQL("drop table if exists "+WORD_TABLE_NAME);
            onCreate(db);
        }
    }
    public DataBaseHelperAdapter dbOpen() throws SQLException
    {
        db = dbhelper.getWritableDatabase();
        return this;
    }
    //for saved words
    public void insertRecord(String data)
    {
        ContentValues values = new ContentValues();
        values.put(KEY_DATA, data);
        db.insert(TABLE_NAME, null, values);
    }
    public ArrayList<AllDictionaryWords> getAllSavedWords(){
        ArrayList<AllDictionaryWords> list = new ArrayList<AllDictionaryWords>();
        String selectQuery = "SELECT * FROM "+TABLE_NAME;
        Cursor cursor = db.rawQuery(selectQuery,null);
        if(cursor.moveToFirst()){
            do{
                AllDictionaryWords current = new AllDictionaryWords();
                current.setWord_id(cursor.getInt(0));
                current.setmValue(cursor.getString(1));
                list.add(current);
            }while (cursor.moveToNext());
        }
        return list;
    }
    public void deleteSavedWord(int position) {
        db = dbhelper.getWritableDatabase();
        String delete = "DELETE FROM " + TABLE_NAME + " WHERE " + KEY_ID + " = '" + position + "' ";
        Log.d("Delete Query", delete);
        db.execSQL(delete);
        db.close();
    }
    //for all words
    public void insertAllWords(String word){
        ContentValues contentValues = new ContentValues();
        contentValues.put(WORD_KEY_DATA,word);
        db.insert(WORD_TABLE_NAME,null,contentValues);
    }
    public boolean checkForTables(){
        boolean hasTables = false;
        Cursor cursor = db.rawQuery("SELECT * FROM " +WORD_TABLE_NAME, null);

        if(cursor != null && cursor.getCount() > 0){
            hasTables=true;
            cursor.close();
        }

        return hasTables;
    }
    public void close()
    {
        dbhelper.close();
    }
}
