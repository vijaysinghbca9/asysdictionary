package com.androidbelieve.AsysDic;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by omnipresent on 27/11/15.
 */
public abstract class WebApiCall extends AsyncTask<Void, Integer,String> {

    private long totalSize=0;
    private ArrayList<BasicNameValuePair> mBasicNameValuePairs=new ArrayList<BasicNameValuePair>();
    private String mUrl;
    int WID;

    protected static Context mContext;

//    protected abstract void onUploadStarted();
//    protected abstract void onUploadProgressChange(int progress);
    protected abstract void onUploadProgressFinishedSuccess(String result);
//    protected abstract void onUploadProgressFinishedfail();


    private ProgressDialog progressDialog;

    private boolean mIsProgressVisible;
    OnAddPostListioner onAddPostListioner;

    @SuppressLint("NewApi") public WebApiCall(Context context,String url,ArrayList<BasicNameValuePair> basicNameValuePairs,boolean isProgressVible,int id) {

        mContext=context;
        mIsProgressVisible=isProgressVible;
        WID = id;

        if(mIsProgressVisible){
            progressDialog=new ProgressDialog(mContext);
            progressDialog.setMessage("Please Wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        //mUrl=url;
        mUrl = "http://infrasissolution.com/APIs/dictionaryapp/wordofmeaning.php?";

        JSONObject js = new JSONObject();
        try {
            js.put("wordid","100001740");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        basicNameValuePairs.add(new BasicNameValuePair("Request", js.toString()));
        mBasicNameValuePairs=basicNameValuePairs;

        mBasicNameValuePairs=basicNameValuePairs;
        mContext=context;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //onUploadStarted();
    }
    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        //AppUtill.showLog("Uploading progress===> "+values[0]+"%");
        //onUploadProgressChange(values[0]);
    }

    @Override
    protected String doInBackground(Void... params) {
        Log.e(mBasicNameValuePairs + "'s size is", "" + mBasicNameValuePairs.size());

        /*if(mBasicNameValuePairs.size() > 0){

            return uploadFile(mBasicNameValuePairs);
        }
        else
        {*/
                    //mBasicNameValuePairs.add(new BasicNameValuePair("Request", ""));
            return getFile();
        //}
    }

    @SuppressLint("NewApi") @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if(mIsProgressVisible){
            progressDialog.cancel();
        }
        //AppUtill.showLog(result);
        Log.e("Received",result);

        /*if(result!=null){
            if(!result.isEmpty()){

                Log.e("Recived",result);
            }else{
               Log.e("Recived",result);
            }
        }else{
            Log.e("Recived","Empty");
        }*/
        System.gc();

    }
    @SuppressWarnings("deprecation")
    private String getFile(){
        InputStream inputStream = null;
        String result = "";
        URL url= null;
        HttpURLConnection urlConnection = null;
        try {
            url = new URL("http://infrasissolution.com/APIs/dictionaryapp/wordofmeaning.php?Request={\"wordid\":\""+WID+"\"}");
            urlConnection = (HttpURLConnection)url.openConnection();

            inputStream = (InputStream) urlConnection.getContent();
            String response = convertInputStreamToString(inputStream);
            result = response;

//            int data = isr.read();
//            while (data != -1) {
//                char current = (char) data;
//                data = isr.read();
//                System.out.print(current);
//            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
            /*try {

//            / create Apache HttpClient /
                    HttpClient httpclient = new DefaultHttpClient();

//            / HttpGet Method /

            HttpGet httpGet = new HttpGet(mUrl);

//            / optional request header /
//                    httpGet.setHeader("Content-Type", "application/json");
//
////            / optional request header /
//                    httpGet.setHeader("Accept", "application/json");

//            / Make http request call /
            HttpResponse httpResponse = httpclient.execute(httpGet);
            int statusCode = httpResponse.getStatusLine().getStatusCode();

//            / 200 represents HTTP OK /
            if (statusCode ==  200) {
//                / receive response as inputStream /
                inputStream = httpResponse.getEntity().getContent();
                String response = convertInputStreamToString(inputStream);
                result = response;


            } else{
                Log.w("status","not found");

            }
        } catch (Exception e) {

        }*/
        return result; //"Failed to fetch data!";
    }

    // convert inputstream to String
    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    @SuppressWarnings("deprecation")
    private String uploadFile(ArrayList<BasicNameValuePair> basicNameValuePairs) {
        String responseString = null;

        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(mUrl);
        try {
//            AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
//                    new RecoverySystem.ProgressListener() {
//
//                        @Override
//                        public void transferred(long num) {
//                            publishProgress((int) ((num / (float) totalSize) * 100));
//                        }
//                    });
//
//
//            for (int i = 0; i < basicNameValuePairs.size(); i++) {
//                entity.addPart(basicNameValuePairs.get(i).getName(),new StringBody(basicNameValuePairs.get(i).getValue()));
//            }
//
//
//            totalSize = entity.getContentLength();
//            System.out.println("Total Length of the file ===> " + totalSize);
//            httppost.setEntity(entity);

            // Making server call
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity r_entity = response.getEntity();

            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                // Server response
                responseString = EntityUtils.toString(r_entity);
            } else {
                responseString = "Error occurred! Http Status Code: "
                        + statusCode;
            }

        } catch (ClientProtocolException e) {
            responseString = e.toString();
            e.printStackTrace();
        } catch (IOException e) {
            responseString = e.toString();
            e.printStackTrace();
        }

        //AppUtill.showLog("Final Response===> "+responseString);
        return responseString;

    }
}