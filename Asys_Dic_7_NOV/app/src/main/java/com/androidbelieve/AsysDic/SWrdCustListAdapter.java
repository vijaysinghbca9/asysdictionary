package com.androidbelieve.AsysDic;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import modelclasses.AllDictionaryWords;

public class SWrdCustListAdapter extends BaseAdapter {

    private final Activity context;
    ArrayList<AllDictionaryWords> data;
    LayoutInflater inflater;

    public SWrdCustListAdapter(Activity context, ArrayList<AllDictionaryWords> d) {
        // TODO Auto-generated constructor stub

        this.context=context;
        this.data = d;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position,View view,ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.wrd_frg_list_row, null);

        AllDictionaryWords model = data.get(position);

        final TextView txtTitle = (TextView) rowView.findViewById(R.id.txtWrdList);
        txtTitle.setText(model.getmValue());

        return rowView;

    }
}