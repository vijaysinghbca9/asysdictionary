package com.androidbelieve.AsysDic;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.sql.SQLException;
import java.util.ArrayList;

import modelclasses.AllDictionaryWords;
import modelclasses.DataBaseHelperAdapter;

/**
 * Created by Dakshesh on 07/11/15.
 */
public class SvdWrdsFragment extends Fragment {


    SWrdCustListAdapter SimllarWordCustomAdapter;
    ArrayList<AllDictionaryWords> strArray;
    DataBaseHelperAdapter dba;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.saved_wrds_frg, null);
        ListView lv = (ListView) view.findViewById(R.id.list_svd_wrd);
        strArray = new ArrayList<AllDictionaryWords>();
        SimllarWordCustomAdapter = new SWrdCustListAdapter(getActivity(),strArray);
        dba = new DataBaseHelperAdapter(getActivity());
        lv.setAdapter(SimllarWordCustomAdapter);
        registerForContextMenu(lv);

        setUpSaveListView();
        return view;
    }
    //context menu
    protected static final int CONTEXTMENU_OPTION1 = 1;

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Select The Action");
        menu.add(Menu.NONE, CONTEXTMENU_OPTION1, 0, "Delete");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        if(item.getTitle()=="Delete"){
            final AdapterView.AdapterContextMenuInfo DelMenuInfo = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();

            AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
            adb.setMessage("are you sure you want to delete?");
            adb.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    int pos = DelMenuInfo.position;
                    int i =strArray.get(pos).getWord_id();
                    try {
                        dba.dbOpen();
                        dba.deleteSavedWord(i);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    strArray.clear();
                    setUpSaveListView();

                }
            });
            adb.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog ad = adb.create();
            ad.show();
        }
        else{
            return false;
        }
        return true;
    }

    public void setUpSaveListView()
    {
        try{
            dba.dbOpen();
            ArrayList<AllDictionaryWords> tempPastiArray = dba.getAllSavedWords();
            for(int i = 0;i < tempPastiArray.size();i++)
            {
                strArray.add(tempPastiArray.get(i));
            }
            SimllarWordCustomAdapter.notifyDataSetChanged();
            dba.close();
        }catch (Exception e){

        }
    }
}
