package com.androidbelieve.AsysDic;

import android.widget.TextView;

/**
 * Created by keyur on 26-01-2016.
 */
public class MySingleton
{
    private static MySingleton instance;

    public String textCopiedValue="";
    public TextView textView = null;

    public static void initInstance()
    {
        if (instance == null)
        {
            // Create the instance
            instance = new MySingleton();
        }
    }

    public static MySingleton getInstance()
    {
        // Return the instance
        return instance;
    }

    private MySingleton()
    {
        // Constructor hidden because this is a singleton
    }

    public void customSingletonMethod()
    {
        // Custom method
    }
}
