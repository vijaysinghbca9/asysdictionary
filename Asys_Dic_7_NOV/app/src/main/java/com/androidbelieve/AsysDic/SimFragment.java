package com.androidbelieve.AsysDic;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

/**
 * Created by Ratan on 7/29/2015.
 */
public class SimFragment extends Fragment {
    static CustomListAdapter SimlistAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.similar_wrds_layout,null);

        ListView lv = (ListView) view.findViewById(R.id.simi_wrds_list);
        SimlistAdapter = new CustomListAdapter(getActivity(), TabFragment.simArrSet);
        lv.setAdapter(SimlistAdapter);

        return view;
    }


}
