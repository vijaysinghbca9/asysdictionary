package com.androidbelieve.AsysDic;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

/**
 * Created by Ratan on 7/29/2015.
 */
public class OpposWrdsFragment extends Fragment {
    public static CustomListAdapter OplistAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.oppos_words_layout,null);

        ListView lv = (ListView) view.findViewById(R.id.oppos_wrd_list);
        String arr[] = getResources().getStringArray(R.array.oppos_wrds);
        //String listData[] = getResources().getStringArray(R.array.autoFill);
        OplistAdapter = new CustomListAdapter(getActivity(), TabFragment.opposArrSet);
        lv.setAdapter(OplistAdapter);

        return view;
    }
}
