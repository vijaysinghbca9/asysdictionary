package com.androidbelieve.AsysDic;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import modelclasses.AllDictionaryWords;

public class MeaningFragment extends Fragment {

    static CustomListAdapter listAdapter,NounListAdapter,SeeAlsoListAdapter;
    public static ListView lv;
    boolean mFlag;
    ArrayList<AllDictionaryWords> myGetData;
    static String arr[];
    static TextView txtGloss,txtNoun,txtSee;
    static ListView nounList,seeAlso;
    SharedPreferences sharedpreferences;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.meaning_layout, null);

        sharedpreferences = getActivity().getSharedPreferences("SharedPref", Context.MODE_PRIVATE);

        lv = (ListView) view.findViewById(R.id.mean_list);
        nounList = (ListView)view.findViewById(R.id.adtrNounListView);
        seeAlso = (ListView)view.findViewById(R.id.seeAlsoListView);
        txtGloss = (TextView)view.findViewById(R.id.txtGloss);
        txtNoun = (TextView)view.findViewById(R.id.txtNoun);
        txtSee = (TextView)view.findViewById(R.id.txtSeeAlso);

        lv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                    // Disallow the touch request for parent scroll on touch of child view
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    return false;
                }
        });
        nounList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // Disallow the touch request for parent scroll on touch of child view
                view.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        seeAlso.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // Disallow the touch request for parent scroll on touch of child view
                view.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        myGetData = new ArrayList<AllDictionaryWords>();
        arr = getResources().getStringArray(R.array.mean);
        listAdapter = new CustomListAdapter(getActivity(), TabFragment.arrSet);
        NounListAdapter = new CustomListAdapter(getActivity(),TabFragment.nounSet);
        SeeAlsoListAdapter = new CustomListAdapter(getActivity(),TabFragment.seeAlsoSet);
        lv.setAdapter(listAdapter);
        nounList.setAdapter(NounListAdapter);
        seeAlso.setAdapter(SeeAlsoListAdapter);
        String saveTextSize = sharedpreferences.getString("FontSize","14");
        if(TabFragment.arrSet.size() != 0){
            MeaningFragment.txtGloss.setVisibility(View.VISIBLE);
            txtGloss.setTextSize(Float.parseFloat(saveTextSize));
        }
        if(TabFragment.nounSet.size() !=0){
            MeaningFragment.txtNoun.setVisibility(View.VISIBLE);
            txtNoun.setTextSize(Float.parseFloat(saveTextSize));
        }
        if(TabFragment.seeAlsoSet.size() != 0){
            MeaningFragment.txtSee.setVisibility(View.VISIBLE);
            txtSee.setTextSize(Float.parseFloat(saveTextSize));
        }
        setUpListView();

        return view;
    }

    public void setListAdapter(){
        if(mFlag == true){
            lv.setAdapter(listAdapter);
        }else {
            lv.setAdapter(null);
        }
    }

    public void setUpListView(){
        ArrayList<AllDictionaryWords> tempArray = TabFragment.arrSet ;
        for(int i =0;i<tempArray.size();i++){
            myGetData.add(tempArray.get(i));
        }
        listAdapter.notifyDataSetChanged();
    }


}
