package com.androidbelieve.AsysDic;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Spinner;

public class SettingFragment extends Fragment {

    Spinner fontSize;
    CheckBox meanLookup;
    boolean isCheckedFlag;
    SharedPreferences sharedpreferences;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.settings_fragment, null);
        fontSize = (Spinner)view.findViewById(R.id.spnr_font_settings);
        meanLookup = (CheckBox)view.findViewById(R.id.autoMeanLookupFlag);
        sharedpreferences = getActivity().getSharedPreferences("SharedPref", Context.MODE_PRIVATE);

        final RadioButton hindiRadioButton = (RadioButton) view.findViewById(R.id.hindiRadioButton);
        final RadioButton englishRadioButton =(RadioButton)view.findViewById(R.id.englishRadioButton);
        hindiRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Hindi Language not available yet please try it later")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    hindiRadioButton.setChecked(false);
                                    englishRadioButton.setChecked(true);
                                    dialog.dismiss();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        });





        String autoFind = sharedpreferences.getString("AutoLookup","false");
        if(autoFind.equals("true")){
            meanLookup.setChecked(true);
        }
        else{
            meanLookup.setChecked(false);
        }


        meanLookup.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    isCheckedFlag = b;
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("AutoLookup", "" + isCheckedFlag);
                    Log.d("Flag",""+isCheckedFlag);
                    editor.commit();
            }
        });

        String strFontSelection = sharedpreferences.getString("FontSelection","1");
        fontSize.setSelection(Integer.parseInt(strFontSelection));

        fontSize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("FontSize", "" + adapterView.getSelectedItem().toString());
                    editor.putString("FontSelection",""+i);
                    editor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        return view;
    }
}
