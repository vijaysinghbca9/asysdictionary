package com.androidbelieve.AsysDic;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import modelclasses.Group;

/**
 * Created by omnipresent on 12/12/15.
 */
public class NewMeaningFrag extends Fragment {
    static modelclasses.ExpandableListAdapter ExpAdapter;
    private ArrayList<Group> ExpListItems;
    static ExpandableListView ExpandList;
    static ArrayList<String> dataforSet;



    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    static List<String> listDataHeader;
    static HashMap<String, List<String>> listDataChild;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_meaning_layout, null);
        //start method preparedata
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        Log.e("NewMeanFrag", "OnCreateCalled");

        listDataHeader.add("GLOSS");
        listDataHeader.add("NOUN");
        listDataHeader.add("SEE ALSO..");

        // Adding child data


        listDataChild.put(listDataHeader.get(0), TabFragment.glossData); // Header, Child data
        listDataChild.put(listDataHeader.get(1), TabFragment.nounData);
        listDataChild.put(listDataHeader.get(2), TabFragment.seeAlsoData);
        //end
        ExpandList = (ExpandableListView) view.findViewById(R.id.exp_list);
        ExpAdapter = new modelclasses.ExpandableListAdapter(getActivity(), listDataHeader,listDataChild);
        ExpandList.setAdapter(ExpAdapter);
        dataforSet = new ArrayList<String>();
        return view;
    }




}
