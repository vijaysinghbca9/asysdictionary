package com.androidbelieve.AsysDic;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import modelclasses.AllDictionaryWords;

public class CustomListAdapter extends BaseAdapter {

    private final Activity context;
    ArrayList<AllDictionaryWords> data;
    private static LayoutInflater inflater;
    TextView txtTitle;
    SharedPreferences sharedpreferences;


    public static int flag = 0;

    public CustomListAdapter(Activity context, ArrayList<AllDictionaryWords> d) {
        //super(context, R.layout.list_row, itemname);
        // TODO Auto-generated constructor stub
        this.context=context;
        data = d;

        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position,View view,ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.list_row, null);
        sharedpreferences = context.getSharedPreferences("SharedPref", Context.MODE_PRIVATE);
        String fontSize = sharedpreferences.getString("FontSize","14");

        final AllDictionaryWords model = data.get(position);
        txtTitle = (TextView) rowView.findViewById(R.id.txtList);

//        if(flag == 1) {
        txtTitle.setText((position+1)+". "+model.getmValue());
        txtTitle.setTextSize(Float.parseFloat(fontSize));
//        }
//        else
//        {
//            txtTitle.setText("");
//        }


        return rowView;
    }
}