package com.androidbelieve.AsysDic;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import modelclasses.AllDictionaryWords;
import modelclasses.DataBaseHelperAdapter;

/**
 * Created by Ratan on 7/27/2015.
 */
public class TabFragment extends Fragment {

    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static int int_items = 3 ;
    static AutoCompleteTextView actv;
    ArrayList<BasicNameValuePair> basicNameValuePairs;
    OnAddPostListioner onAddPostListioner;
    static ArrayList<AllDictionaryWords> arrSet,nounSet,simArrSet,opposArrSet,seeAlsoSet;

    LinearLayout saveLayout;
    TextView TextForSave;
    ImageView saveImage;
    String EnText;
    String[] Wordarr;
    static ArrayAdapter<?> adapter;
    List<String> terms;
    ArrayList<BasicNameValuePair> mBasicNameValuePairs;

    static List<String> glossData, nounData, seeAlsoData;

    SharedPreferences sharedpreferences;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        sharedpreferences = getActivity().getSharedPreferences("SharedPref", Context.MODE_PRIVATE);

        View x =  inflater.inflate(R.layout.tab_layout,null);
        tabLayout = (TabLayout) x.findViewById(R.id.tabs);
        viewPager = (ViewPager) x.findViewById(R.id.viewpager);
        actv = (AutoCompleteTextView)x.findViewById(R.id.txtSearch);
        saveLayout = (LinearLayout)x.findViewById(R.id.LayoutForSave);
        TextForSave = (TextView)x.findViewById(R.id.textForSave);
        saveImage = (ImageView)x.findViewById(R.id.imgSaveWord);


        nounSet = new ArrayList<AllDictionaryWords>();
        arrSet = new ArrayList<AllDictionaryWords>();
        simArrSet = new ArrayList<AllDictionaryWords>();
        opposArrSet = new ArrayList<AllDictionaryWords>();
        seeAlsoSet = new ArrayList<AllDictionaryWords>();

        glossData = new ArrayList<String>();

        nounData = new ArrayList<String>();

        seeAlsoData = new ArrayList<String>();

        basicNameValuePairs = new ArrayList<BasicNameValuePair>();

        onAddPostListioner = new OnAddPostListioner() {
            @Override
            public void addSuccessfull(String postId) {
                Log.d("Message",postId);
            }

            @Override
            public void onFail(String errorMessage) {
                Log.d("Message ::","Faliled");
            }
        };


        saveImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DataBaseHelperAdapter dba = new DataBaseHelperAdapter(getActivity());
                try {
                    dba.dbOpen();
                    dba.insertRecord(TextForSave.getText().toString());
                    Toast.makeText(getActivity(), "Word Saved", Toast.LENGTH_SHORT).show();
                    dba.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }


            }
        });
        terms = new ArrayList<String>();
        Wordarr = terms.toArray(new String[terms.size()]);
        adapter = new ArrayAdapter<Object>(getActivity(),android.R.layout.simple_list_item_1,Wordarr);
        actv.setAdapter(adapter);
        actv.addTextChangedListener(SearchWatcher);
        actv.setThreshold(2);

        actv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                EnText = adapterView.getItemAtPosition(i).toString();
                if (EnText.equals("")) {
                    actv.setError("Enter Word");

                } else {
                    searchEnteredWord(EnText);
                }

            }
        });
        actv.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if ( (keyEvent.getAction() == KeyEvent.ACTION_DOWN  ) &&
                        (i == KeyEvent.KEYCODE_ENTER)   )
                {
                    EnText = actv.getText().toString();
                    searchEnteredWord(actv.getText().toString());
                    return true;
                }
                return false;
            }
        });
        ImageView search = (ImageView)x.findViewById(R.id.search_text_ic);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EnText = actv.getText().toString();
                if (EnText.equals("")){
                    actv.setError("Enter Word");

                }else {
                    searchEnteredWord(EnText);
                }
            }
        });

        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));

        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                    tabLayout.setupWithViewPager(viewPager);
                   }
        });
        if(MySingleton.getInstance().textCopiedValue.length() > 0){
            actv.setText(MySingleton.getInstance().textCopiedValue);
        }
        return x;

    }

    class MyAdapter extends FragmentPagerAdapter{

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment with respect to Position .
         */

        @Override
        public Fragment getItem(int position)
        {
          switch (position){
              case 0 : return new NewMeaningFrag();
              case 1 : return new SimFragment();
              case 2 : return new OpposWrdsFragment();
          }
        return null;
        }

        @Override
        public int getCount() {

            return int_items;

        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position){
                case 0 :
                    return "MEANING";
                case 1 :
                    return "SIMILAR WORDS";
                case 2 :
                    return "OPPOSITE WORDS";
            }
                return null;
        }
    }


    //Text Search Watcher
    private final TextWatcher SearchWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            // For Auto Complete
            String autoFind = sharedpreferences.getString("AutoLookup","false");
            Log.d("AutoFind",""+autoFind);
            if(autoFind.equals("true")){
                if(actv.getText().toString().length() == 2){
                    EnText = actv.getText().toString();
                    stopTask();
                    doTimerTask();
                }else{
                }
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    //For Search Text WEbService
    public class TextSearchAsync extends AsyncTask<Void, Integer,String>{
        String mUrl;
        boolean mIsProgressVisible;
        Context mContext;
        private ProgressDialog progressDialog;

        public TextSearchAsync(Context context,String url,boolean isProgressVisible){
            mIsProgressVisible = isProgressVisible;
            mContext = context;
            mUrl = url;

            if(mIsProgressVisible){
                progressDialog=new ProgressDialog(context);
                progressDialog.setMessage("Please Wait");
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected void onPostExecute(String s) {
            String word;
            if(mIsProgressVisible){
                progressDialog.cancel();
            }
            super.onPostExecute(s);
            try{
                JSONObject jsonObject = new JSONObject(s);
                Log.e("TextSearch",s);
                String jStaus = jsonObject.get("status").toString();
                Log.e("status ",jStaus);
                String searchFor;
                if (jStaus.equals("true")){
                    JSONArray jsonArray = jsonObject.optJSONArray("data");
                    for(int i =0;i<jsonArray.length();i++){
                        JSONObject mObj = jsonArray.getJSONObject(i);
                        searchFor = mObj.getString("word");
                        Log.e("New Text Search",searchFor);

//                        searchFor = jsonArray.getString(i);
                            terms.add(i, searchFor);
                    }
                    Wordarr = terms.toArray(new String[terms.size()]);
                    adapter = new ArrayAdapter<Object>(getActivity(),android.R.layout.simple_list_item_1,Wordarr);
                    adapter.notifyDataSetChanged();
                    actv.setAdapter(adapter);
                }
                else{
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
            @Override
        protected String doInBackground(Void... voids) {
            InputStream inputStream = null;
            String result = "";
            URL url= null;
            HttpURLConnection urlConnection = null;
            String dataUrlParameters = "keyword="+EnText;
            HttpURLConnection connection = null;
            try {
// Create connection
                url = new URL(mUrl);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
                connection.setRequestProperty("Content-Length","" + Integer.toString(dataUrlParameters.getBytes().length));
                connection.setRequestProperty("Content-Language", "en-US");
                connection.setUseCaches(false);
                connection.setDoInput(true);
                connection.setDoOutput(true);
// Send request
                DataOutputStream wr = new DataOutputStream(
                        connection.getOutputStream());
                wr.writeBytes(dataUrlParameters);
                wr.flush();
                wr.close();
// Get Response
                InputStream is = connection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                    response.append('\r');
                }
                rd.close();
                String responseStr = response.toString();
                result = responseStr;

            } catch (Exception e) {

                e.printStackTrace();

            } finally {

                if (connection != null) {
                    connection.disconnect();
                }
            }
            return  result;
        }
    }
    //********New WS Class******
    public class NewReqAsync extends AsyncTask<Void, Integer,String>{

        String mUrl;
        boolean mIsProgressVisible;
        Context mContext;
        private ProgressDialog progressDialog;

        public NewReqAsync(Context context,String url,boolean isProgressVisible){

            mUrl = url;
            mIsProgressVisible = isProgressVisible;
            mContext = context;

            if(mIsProgressVisible){
                progressDialog=new ProgressDialog(context);
                progressDialog.setMessage("Please Wait");
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            String noun,seeAlso;
            List<String> meanWord = null;
            if(mIsProgressVisible){
                progressDialog.cancel();
            }
            Log.e("Result of new", s);
            try{
                JSONObject jsonObject = new JSONObject(s);
                String jStaus = jsonObject.get("status").toString();
                Log.e("status ",jStaus);
                Log.e("New Data ",s);
                AllDictionaryWords dicModel = null;
                AllDictionaryWords nounModel = null;
                AllDictionaryWords seeAlsoModel = null;
                if (jStaus.equals("true")) {
                    JSONObject innerJsonObj = jsonObject.getJSONObject("data");
                    Log.e("InnerJsonObject",innerJsonObj.toString());

                    JSONArray jsonArray = innerJsonObj.optJSONArray("similar");
                    for(int i =0;i < jsonArray.length();i++){
                        JSONObject inObj = jsonArray.optJSONObject(i);
                        String data = inObj.getString("gloss").toString();
                        Log.d("Data",data);
                        dicModel = new AllDictionaryWords();
                        dicModel.setmValue(data);
                        glossData.add(data);
                    }
                    NewMeaningFrag.ExpandList.expandGroup(0);

                    //get noun of word
                    JSONArray jsonNounArray = innerJsonObj.optJSONArray("adtr_noun");
                    for (int i = 0; i < jsonNounArray.length(); i++) {
                        nounModel = new AllDictionaryWords();
                        JSONObject innerJobj = jsonNounArray.getJSONObject(i);
                        noun = innerJobj.get("gloss").toString();
                        Log.d("New Noun", noun);
                        nounModel.setmValue(noun);
                        nounData.add(noun);
                        Log.e("Adtr Noun", noun);
                        nounSet.add(nounModel);
                    }
                    NewMeaningFrag.ExpandList.expandGroup(1);
//                    MeaningFragment.NounListAdapter.notifyDataSetChanged();
//                    if(nounSet.size() != 0){
//                        MeaningFragment.txtNoun.setVisibility(View.VISIBLE);
//                    }
                    JSONArray seeAlsoJArray = innerJsonObj.getJSONArray("seealso");
                    for (int i = 0; i < seeAlsoJArray.length(); i++) {
                        seeAlsoModel = new AllDictionaryWords();
                        JSONObject innerJObj = seeAlsoJArray.getJSONObject(i);
                        seeAlso = innerJObj.get("gloss").toString();
                        Log.d("SeeAlso", seeAlso);
                        seeAlsoModel.setmValue(seeAlso);
                        seeAlsoData.add(seeAlso);
                        seeAlsoSet.add(seeAlsoModel);
                    }
                    NewMeaningFrag.ExpandList.expandGroup(2);
                    MeaningFragment.SeeAlsoListAdapter.notifyDataSetChanged();

                    if(seeAlsoSet.size() != 0){
                        MeaningFragment.txtSee.setVisibility(View.VISIBLE);
                    }
                    CustomListAdapter.flag = 1;
                    NewMeaningFrag.ExpAdapter.notifyDataSetChanged();

                }
                else{
                    Toast.makeText(mContext,"No Data Found for "+EnText,Toast.LENGTH_SHORT).show();
                    CustomListAdapter.flag = 0;
                    MeaningFragment.listAdapter.notifyDataSetChanged();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }
        @Override
        protected String doInBackground(Void... voids) {
            InputStream inputStream = null;
            String result = "";
            URL url= null;
            HttpURLConnection urlConnection = null;
            String dataUrlParameters = "wordid="+EnText;
            HttpURLConnection connection = null;
            try {
// Create connection
                url = new URL(mUrl);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
                connection.setRequestProperty("Content-Length","" + Integer.toString(dataUrlParameters.getBytes().length));
                connection.setRequestProperty("Content-Language", "en-US");
                connection.setUseCaches(false);
                connection.setDoInput(true);
                connection.setDoOutput(true);
// Send request
                DataOutputStream wr = new DataOutputStream(
                        connection.getOutputStream());
                wr.writeBytes(dataUrlParameters);
                wr.flush();
                wr.close();
// Get Response
                InputStream is = connection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                    response.append('\r');
                }
                rd.close();
                String responseStr = response.toString();
                result = responseStr;

            } catch (Exception e) {

                e.printStackTrace();

            } finally {

                if (connection != null) {
                    connection.disconnect();
                }
            }
            return  result;
        }
    }

    //Similar Words
    public class SimilarAync extends AsyncTask<Void, Integer,String>{

        String mUrl;

        public SimilarAync(String url){

            mUrl = url;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected void onPostExecute(String s) {
            String word;
            List<String> allSimWord = null;
            super.onPostExecute(s);
            Log.e("********","**********");
            Log.e("Result of similar", s);
            try{
                JSONObject jsonObject = new JSONObject(s);
                String jStaus = jsonObject.get("status").toString();
                Log.e("status ",jStaus);
                AllDictionaryWords simModel = null;
                if (jStaus.equals("true")){
                    JSONArray jsonArray = jsonObject.optJSONArray("data");
                    for(int i =0;i<jsonArray.length();i++){
                        simModel = new AllDictionaryWords();
                        JSONObject innerJobj = jsonArray.getJSONObject(i);
                        int id = Integer.parseInt(innerJobj.optString("synset_id").toString());
                        word = innerJobj.optString("word").toString();
                        Log.e("synset_id", "" + id);
                        Log.e("word", word);
                        Log.e("Sim Data Size", "" + simArrSet.size());
                        simModel.setmValue(word);
                        simArrSet.add(simModel);
                        SimFragment.SimlistAdapter.notifyDataSetChanged();
                    }
                    CustomListAdapter.flag  = 1;

                }
                else{
                    //CustomListAdapter.flag = 0;
                    Toast.makeText(getActivity(),"No Similar words Found for "+EnText,Toast.LENGTH_SHORT).show();
                    SimFragment.SimlistAdapter.notifyDataSetChanged();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(Void... voids) {
            InputStream inputStream = null;
            String result = "";
            URL url= null;
            HttpURLConnection urlConnection = null;
            try {
                url = new URL(mUrl);
                urlConnection = (HttpURLConnection)url.openConnection();
                inputStream = (InputStream) urlConnection.getContent();
                String response = convertInputStreamToString(inputStream);
                result = response;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return  result;
        }
    }
    public class OpposAsync extends AsyncTask<Void, Integer,String>{

        String mUrl;

        public OpposAsync(String url){

            mUrl = url;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected void onPostExecute(String s) {
            String word;
            List<String> allSimWord = null;
            super.onPostExecute(s);
            Log.e("********","**********");
            Log.e("Result of Oppos",s);
            try{
                JSONObject jsonObject = new JSONObject(s);
                String jStaus = jsonObject.get("status").toString();
                Log.e("status ",jStaus);
                AllDictionaryWords OpposModel;
                if (jStaus.equals("true")){
                    JSONArray jsonArray = jsonObject.optJSONArray("data");
                    for(int i =0;i<jsonArray.length();i++){
                        OpposModel = new AllDictionaryWords();
                        JSONObject innerJobj = jsonArray.getJSONObject(i);
                        int id = Integer.parseInt(innerJobj.optString("synset_id").toString());
                        word = innerJobj.optString("word").toString();
                        Log.e("synset_id", "" + id);
                        Log.e("word", word);
                        Log.e("Sim Data Size", "" + simArrSet.size());
                        OpposModel.setmValue(word);
                        opposArrSet.add(OpposModel);
                    }

                    CustomListAdapter.flag  = 1;
                    OpposWrdsFragment.OplistAdapter.notifyDataSetChanged();

                }
                else{
                    //CustomListAdapter.flag = 0;
                    Toast.makeText(getActivity(),"No Opposite words Found for "+EnText,Toast.LENGTH_SHORT).show();
                    OpposWrdsFragment.OplistAdapter.notifyDataSetChanged();

                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(Void... voids) {
            InputStream inputStream = null;
            String result = "";
            URL url= null;
            HttpURLConnection urlConnection = null;
            try {
                url = new URL(mUrl);
                urlConnection = (HttpURLConnection)url.openConnection();
                inputStream = (InputStream) urlConnection.getContent();
                String response = convertInputStreamToString(inputStream);
                result = response;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return  result;
        }
    }

    private String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }
    public void hideSoftKeyboard() {
        if(getActivity().getCurrentFocus()!=null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void searchEnteredWord(String enteredText){
        arrSet.clear();
        simArrSet.clear();
        opposArrSet.clear();
//        MeaningFragment.NounListAdapter.notifyDataSetChanged();
//        MeaningFragment.SeeAlsoListAdapter.notifyDataSetChanged();
//        MeaningFragment.NounListAdapter.notifyDataSetChanged();
        glossData.clear();
        nounData.clear();
        seeAlsoData.clear();
        NewMeaningFrag.ExpAdapter.notifyDataSetChanged();

        hideSoftKeyboard();

        String strUrl = "http://infrasissolution.com/APIs/dictionaryapp/wordofmeaning.php";
        String similarUrl = "http://infrasissolution.com/APIs/dictionaryapp/similarwordofmeaning.php?Request={\"wordid\":\""+enteredText+"\"}";
                          // http://infrasissolution.com/APIs/dictionaryapp/similarwordofmeaning.php
        String opposUrl = "http://infrasissolution.com/APIs/dictionaryapp/oppositeword.php?Request={\"wordid\":\""+enteredText+"\"}";

        new NewReqAsync(getActivity(), strUrl, true).execute();
        new SimilarAync(similarUrl).execute();
        new OpposAsync(opposUrl).execute();

        saveLayout.setVisibility(View.VISIBLE);
        TextForSave.setText(EnText);
        String saveTextSize = sharedpreferences.getString("FontSize","14");
        TextForSave.setTextSize(Float.parseFloat(saveTextSize));
    }
    public void stopTask(){

        if(mTimerTask!=null){
            Log.d("TIMER", "timer canceled");
            mTimerTask.cancel();
        }

    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            if(msg.what==100){
                if(actv!=null){
                    if(actv.getText().toString().trim().length()!=0){
                        String searchWordUrl = "http://infrasissolution.com/APIs/dictionaryapp/searchword.php";
                        new TextSearchAsync(getActivity(),searchWordUrl,true).execute();
                    }
                }
            }
        }
    };
    private Timer timer;
    private TimerTask mTimerTask;
    public void doTimerTask(){
        timer=new Timer();
        mTimerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {

                        android.os.Message message=new android.os.Message();
                        message.what=100;
                        handler.sendMessage(message);
                        Log.d("TIMER", "TimerTask run");
                    }
                });
            }};
        timer.schedule(mTimerTask, 600);

    }
}