package com.androidbelieve.AsysDic;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import modelclasses.AllDictionaryWords;

public class MainActivity extends AppCompatActivity{
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    ProgressDialog progressDialog;
    String result = null;
    AllDictionaryWords dictionaryWords;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String preview = bundle.getString("keysearch");
            if (preview != null) {

            }
        }
        //new GetAllWordsAsync().execute("http://infrasissolution.com/APIs/dictionaryapp/getdictionarywords.php");
//        progressDialog=new ProgressDialog(this);
//        progressDialog.setMessage("Please Wait");
//        progressDialog.show();
//        progressDialog.setCancelable(false);

        /**
         *Setup the DrawerLayout and NavigationView
         */

             mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
             mNavigationView = (NavigationView) findViewById(R.id.shitstuff) ;

        /**
         * Lets inflate the very first fragment
         * Here , we are inflating the TabFragment as the first Fragment
         */

             mFragmentManager = getSupportFragmentManager();
             mFragmentTransaction = mFragmentManager.beginTransaction();
             mFragmentTransaction.replace(R.id.containerView,new TabFragment()).commit();
        /**
         * Setup click events on the Navigation View Items.
         */

             mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
             @Override
             public boolean onNavigationItemSelected(MenuItem menuItem) {
                mDrawerLayout.closeDrawers();



                 if (menuItem.getItemId() == R.id.nav_dic_tools) {
                     FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                     fragmentTransaction.replace(R.id.containerView,new SettingFragment()).commit();
                     toolbar.setTitle(menuItem.getTitle());

                 }

                if (menuItem.getItemId() == R.id.nav_saved_words) {
                    FragmentTransaction xfragmentTransaction = mFragmentManager.beginTransaction();
                    xfragmentTransaction.replace(R.id.containerView,new SvdWrdsFragment()).commit();
                    toolbar.setTitle(menuItem.getTitle());
                }
                if (menuItem.getItemId() == R.id.nav_home) {
                    FragmentTransaction xfragmentTransaction = mFragmentManager.beginTransaction();
                    xfragmentTransaction.replace(R.id.containerView,new TabFragment()).commit();
                    toolbar.setTitle(R.string.app_name);
                }
                 if(menuItem.getItemId() == R.id.nav_abt_us){
                     FragmentTransaction xfragmentTransaction = mFragmentManager.beginTransaction();
                     xfragmentTransaction.replace(R.id.containerView,new AboutUsFragment()).commit();
                     toolbar.setTitle(menuItem.getTitle());
                 }
                 return false;
            }

        });

        /**
         * Setup Drawer Toggle of the Toolbar
         */


                ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this,mDrawerLayout, toolbar,R.string.app_name,
                R.string.app_name);

                mDrawerLayout.setDrawerListener(mDrawerToggle);

                mDrawerToggle.syncState();
    }
    /*
    public class GetAllWordsAsync extends AsyncTask<String,Void,String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected void onPostExecute(String str) {
            super.onPostExecute(str);
            try {
                JSONObject jsonObject = new JSONObject(result);
                String key,tempStr;
                Iterator<String> iterator = jsonObject.keys();
                while (iterator.hasNext()){
                    key = iterator.next();
                    tempStr = (String) jsonObject.get(key);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            progressDialog.cancel();

        }

        @Override
        protected String doInBackground(String... params) {
            InputStream inputStream = null;
            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(params[0]);
                HttpResponse httpResponse = httpClient.execute(httpGet);
                inputStream = httpResponse.getEntity().getContent();

                if (inputStream != null)
                {       result = convertInputStreamToString(inputStream);
                    //Log.e("Data : ", result);
                }
                else
                    result = "Did not work!";


            }
            catch (Exception e)
            {

            }
            return result;
        }
        private String convertInputStreamToString(InputStream inputStream) throws IOException {
            BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
            String line = "";
            String result = "";
            while((line = bufferedReader.readLine()) != null)
                result += line;

            inputStream.close();
            return result;

        }
    }*/

}