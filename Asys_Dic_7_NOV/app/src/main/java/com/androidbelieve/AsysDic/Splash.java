package com.androidbelieve.AsysDic;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;

import dictionary_services.CopyMyTextService;
import modelclasses.DataBaseHelperAdapter;


/**
 * Created by Dakshesh on 03/11/15.
 */
public class Splash extends Activity {
    protected boolean _active = true;
    protected int _splashTime = 3000;
    String result = null;
    ProgressDialog progressDialog;
    String url = "http://infrasissolution.com/APIs/dictionaryapp/getdictionarywords.php";
    DataBaseHelperAdapter db;

    @Override
    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(R.layout.splash);
        db = new DataBaseHelperAdapter(getApplicationContext());

        Thread splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    db.dbOpen();
                    int waited = 0;
//                    if(db.checkForTables()){
//                        new GetAllWordsAsync().execute(url);
//                    }

                    while (_active && (waited < _splashTime)) {
                        sleep(100);
                        if (_active) {
                            waited += 100;
                        }
                    }
                } catch (Exception e) {

                } finally {
                    startService();
                    startActivity(new Intent(Splash.this,
                            MainActivity.class));
                    finish();
                }
            };
        };
        splashTread.start();
    }

    //Get All Words From Webservice
    public class GetAllWordsAsync extends AsyncTask<String,Void,String> {
        public void GetAllWordsAsync(){
            progressDialog = new ProgressDialog(getApplicationContext());
            progressDialog.setMessage("Please Wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected void onPostExecute(String str) {
            super.onPostExecute(str);
            try {
                JSONObject jsonObject = new JSONObject(result);
                String key,tempStr;
                Iterator<String> iterator = jsonObject.keys();
                while (iterator.hasNext()){
                    key = iterator.next();
                    tempStr =jsonObject.get(key).toString();
                    Log.e("Recieved",tempStr);
                    db.insertAllWords(tempStr);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            progressDialog.cancel();

        }

        @Override
        protected String doInBackground(String... params) {
            InputStream inputStream = null;
            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(params[0]);
                HttpResponse httpResponse = httpClient.execute(httpGet);
                inputStream = httpResponse.getEntity().getContent();

                if (inputStream != null)
                {       result = convertInputStreamToString(inputStream);
                }
                else
                    result = "Did not work!";


            }
            catch (Exception e)
            {

            }
            return result;
        }
        private String convertInputStreamToString(InputStream inputStream) throws IOException {
            BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
            String line = "";
            String result = "";
            while((line = bufferedReader.readLine()) != null)
                result += line;

            inputStream.close();
            return result;

        }
    }

    // Method to start the service
    public void startService() {
        startService(new Intent(getBaseContext(), CopyMyTextService.class));
    }

}
